import React from 'react';
import { getAuth, signOut } from 'firebase/auth';
import { useSelector } from 'react-redux';
import './style.scss';

export interface IHomePageProps {}

const HomePage: React.FC<IHomePageProps> = () => {
    const dataUsers = useSelector((state : any) => state.reducer[0]);
    const auth = getAuth();
    
    return (
        <div className='center-layout'>
            <h2>Thông tin của <span> {dataUsers?.displayName} </span></h2>
            <p>Email: {dataUsers?.email}</p>
            <h3>Ảnh đại diện</h3>
            <img src={`${dataUsers?.photoURL}`} alt="" /> <br />
            <button onClick={() => signOut(auth)}>Sign out</button>
        </div>
    );
};

export default HomePage;
