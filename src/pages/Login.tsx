import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { getAuth, GoogleAuthProvider, signInWithPopup } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';
import './style.scss';
import { addUser } from './Users.slice';

export interface ILoginPageProps {}
const LoginPage: React.FC<ILoginPageProps> = () => {
    const dispatch = useDispatch();
    const auth = getAuth();
    const navigate = useNavigate();
    const [authing, setAuthing] = useState(false);

    const signInWithGoogle = async () => {
        setAuthing(true);
        
        signInWithPopup(auth, new GoogleAuthProvider())
            .then((response) => {      
                dispatch(addUser(response.user));       
                navigate('/');
            })
            .catch((error) => {
                console.log(error);
                setAuthing(false);
            });
    };
    
    return (
        <div className='center-layout'>
            <p>Login Page</p>
            <button onClick={() => signInWithGoogle()} disabled={authing}>
                Sign in with Google
            </button>
        </div>
    );
};

export {LoginPage};
