import { combineReducers } from "redux";
import userReducer from "../pages/Users.slice";

const rootReducer = combineReducers({
    reducer: userReducer
});

export default rootReducer;